package fun.hanlc.config;

import fun.hanlc.model.entity.VerifyEntity;
import fun.hanlc.model.service.VerifyService;
import fun.hanlc.util.ClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

@Configuration
@ConditionalOnClass(VerifyProperties.class)
@ConditionalOnExpression("${fun.hanlc.verify.sys-start:false}")
public class SysStartAutoCinfigure {
    Logger logger = LoggerFactory.getLogger(SysStartAutoCinfigure.class);
    @Resource
    private VerifyService verifyService;


    @PostConstruct
    private void start(){
        logger.info("开始加载数据库文件初始化");
        List<VerifyEntity> list = verifyService.list();
        list.forEach(t ->{
            try {
                ClassUtil.getClass(t.getJavaContent(),t.getPackageName());
            } catch (Exception e) {
                throw new RuntimeException("初始化启动失败");
            }
        });

    }
}
