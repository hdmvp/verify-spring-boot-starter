package fun.hanlc.config;


import fun.hanlc.Constant.PathConstant;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "fun.hanlc.verify")
public class VerifyProperties {

    /**
     * 验证aop路径拦截配置
     */
    private String verifyAopPath;
    /**
     *  验证aop路径拦截配置(多个)
     */
    private List<String> verifyAopPaths;

    private String pathType = PathConstant.ALL.getType();
    /**
     * 是否跟随系统启动初始化
     */
    private Boolean sysStart;

    public Boolean getSysStart() {
        return sysStart;
    }

    public void setSysStart(Boolean sysStart) {
        this.sysStart = sysStart;
    }

    public String getVerifyAopPath() {
        return verifyAopPath;
    }

    public void setVerifyAopPath(String verifyAopPath) {
        this.verifyAopPath = verifyAopPath;
    }

    public List<String> getVerifyAopPaths() {
        return verifyAopPaths;
    }

    public void setVerifyAopPaths(List<String> verifyAopPaths) {
        this.verifyAopPaths = verifyAopPaths;
    }

    public String getPathType() {
        return pathType;
    }

    public void setPathType(String pathType) {
        this.pathType = pathType;
    }
}
