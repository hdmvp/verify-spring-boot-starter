package fun.hanlc.config;

import cn.hutool.core.collection.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConditionalOnClass(VerifyProperties.class)
@EnableConfigurationProperties(VerifyProperties.class)
public class VerifyAutoConfigure {

    private Logger logger = LoggerFactory.getLogger(VerifyAutoConfigure.class);


    @Bean(name = "verifyPathConfig")
    @ConditionalOnMissingBean(name = "verifyPathConfig")
    public String getAopPath(VerifyProperties verifyProperties){
        logger.info("拦截参数校验启动成功");
        return verifyProperties.getVerifyAopPath();
    }
    @Bean(name = "pathsMap")
    @ConditionalOnMissingBean(name = "pathsMap")
    @ConditionalOnExpression("'${fun.hanlc.verify.path-type}'.equals('LIST')")
    public Map<String,Boolean> pathsListToMap(VerifyProperties verifyProperties){
        logger.info("拦截参数为list");
        if (CollectionUtil.isEmpty(verifyProperties.getVerifyAopPaths()) || verifyProperties.getVerifyAopPaths().size() < 1) {
            throw new NullPointerException("请正确配置拦截路径数组");
        }
        Map<String,Boolean> pathsMap = new HashMap<>(verifyProperties.getVerifyAopPaths().size());
        verifyProperties.getVerifyAopPaths().forEach(path ->{
            pathsMap.put(path,true);
        });
        return pathsMap;
    }
}
