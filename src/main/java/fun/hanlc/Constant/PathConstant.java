package fun.hanlc.Constant;

public enum PathConstant {



    ONE("ONE",1L,"onePathInterface"),
    LIST("LIST",2L,"listPathInterface"),
    ALL("ALL",99L,"allPathInterface")
    ;
    private  String type;

    private Long code;

    private String beanName;

    PathConstant(String type, Long code, String beanName) {
        this.type = type;
        this.code = code;
        this.beanName = beanName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
