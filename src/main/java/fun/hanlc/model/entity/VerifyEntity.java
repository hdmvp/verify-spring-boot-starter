package fun.hanlc.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


@TableName("SYS_VERIFY")
public class VerifyEntity {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 动态java代码包名
     */
    @TableField(value = "PACKAGE_NAME")
    private String packageName;
    /**
     * java代码内容
     */
    @TableField(value = "JAVA_CONTENT")
    private String javaContent;
    /**
     * 校验的包名路径 例如 fun.hanlc.xxx
     */
    @TableField(value = "CHECK_PACKAGE_PATH")
    private String checkPackagePath;
    /**
     * 校验的方法名称
     */
    @TableField(value = "METHOD_NAME")
    private String methodName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getJavaContent() {
        return javaContent;
    }

    public void setJavaContent(String javaContent) {
        this.javaContent = javaContent;
    }

    public String getCheckPackagePath() {
        return checkPackagePath;
    }

    public void setCheckPackagePath(String checkPackagePath) {
        this.checkPackagePath = checkPackagePath;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
