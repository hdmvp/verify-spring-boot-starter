package fun.hanlc.model.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.hanlc.model.entity.VerifyEntity;
import fun.hanlc.model.mapper.VerifyMapper;
import org.springframework.stereotype.Service;

@Service
public class VerifyService extends ServiceImpl<VerifyMapper, VerifyEntity> {



    public VerifyEntity findOneByCheckPackagePathAndMethodName(String checkPackagePath,String methodName){
        VerifyEntity verify = lambdaQuery().eq(VerifyEntity::getCheckPackagePath, checkPackagePath)
                .eq(VerifyEntity::getMethodName, methodName).one();
        return verify;
    }


    public VerifyEntity findOneByCheckPackagePath(String checkPackagePath){
        VerifyEntity verify = lambdaQuery().eq(VerifyEntity::getCheckPackagePath, checkPackagePath)
                .one();
        return verify;
    }
}
