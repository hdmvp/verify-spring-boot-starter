package fun.hanlc.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.hanlc.model.entity.VerifyEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VerifyMapper extends BaseMapper<VerifyEntity> {
}
