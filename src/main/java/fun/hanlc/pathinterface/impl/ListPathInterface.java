package fun.hanlc.pathinterface.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.extra.spring.SpringUtil;
import fun.hanlc.pathinterface.PathInterface;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ListPathInterface implements PathInterface {

    private Map<String,Boolean> pathsMap;

    @Override
    public boolean pathCheck(String path) {
        if (CollectionUtil.isEmpty(pathsMap)) {
            pathsMap = SpringUtil.getBean("pathsMap");
        }
        return pathsMap.containsKey(path);
    }
}
