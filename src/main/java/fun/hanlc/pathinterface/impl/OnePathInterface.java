package fun.hanlc.pathinterface.impl;

import fun.hanlc.config.VerifyProperties;
import fun.hanlc.pathinterface.PathInterface;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OnePathInterface implements PathInterface {

    @Resource
    private VerifyProperties verifyProperties;

    @Override
    public boolean pathCheck(String path) {
        return verifyProperties.getVerifyAopPath().equals(path);
    }
}
