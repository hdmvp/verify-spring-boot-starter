package fun.hanlc.pathinterface;

public interface PathInterface {
    /**
     * 请求路径校验
     * @param path
     * @return
     */
    boolean pathCheck(String path);
}
