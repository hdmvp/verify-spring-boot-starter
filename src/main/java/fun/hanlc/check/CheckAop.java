package fun.hanlc.check;


import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import fun.hanlc.Constant.PathConstant;
import fun.hanlc.config.VerifyProperties;
import fun.hanlc.model.entity.VerifyEntity;
import fun.hanlc.model.service.VerifyService;
import fun.hanlc.pathinterface.PathInterface;
import fun.hanlc.util.ClassUtil;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

@Aspect
@Component
public class CheckAop implements ApplicationRunner {

    private Logger log = LoggerFactory.getLogger(CheckAop.class);



    @Resource
    private VerifyService verifyService;

    private PathInterface pathInterface;

    @Resource
    private VerifyProperties verifyProperties;


    //    @Pointcut(value = "(@annotation(org.springframework.web.bind.annotation.PutMapping)||@annotation(org.springframework.web.bind.annotation.PostMapping) || @annotation(org.springframework.web.bind.annotation.RequestMapping) || @annotation(org.springframework.web.bind.annotation.GetMapping)))")
//    @Pointcut("execution(* *(..))")
    @Pointcut("(@within(org.springframework.web.bind.annotation.RestController) || @within(org.springframework.stereotype.Controller)) && (@annotation(org.springframework.web.bind.annotation.RequestMapping) || @annotation(org.springframework.web.bind.annotation.GetMapping) || @annotation(org.springframework.web.bind.annotation.PostMapping) || @annotation(org.springframework.web.bind.annotation.DeleteMapping) || @annotation(org.springframework.web.bind.annotation.PutMapping) || @annotation(org.springframework.web.bind.annotation.PatchMapping) || @annotation(org.springframework.web.bind.annotation.Mapping))")
    private void aopPoint() {
    }


    @Around("aopPoint()")
    public Object doSomething(ProceedingJoinPoint point) throws Throwable {

        Signature sig = point.getSignature();
        Object target = point.getTarget();
        String className = StringUtils.defaultString(target.getClass().getName(), "");
        String methodName = StringUtils.defaultString(sig.getName(), "");
        String params = null;
        try {
            log.info("拦截类{}", className);
            log.info("拦截方法名{}", methodName);
            log.info("参数：{}", JSONUtil.toJsonStr(point.getArgs()));
            params = JSONUtil.toJsonStr(point.getArgs());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        if (pathInterface.pathCheck(className)) {
            // 获取拦截的方法名
            VerifyEntity verifyEntity = verifyService.findOneByCheckPackagePath(className);
            if (verifyEntity != null) {
                Class<?> aClass = ClassUtil.getClass(verifyEntity.getJavaContent(), verifyEntity.getPackageName());
                if (!ClassUtil.isMethodExist(aClass,methodName)) {
                    log.warn("类文件中不存在调用方法名的java方法");
                }else {
                    log.info("开始调用java动态方法");
                    Object instance = aClass.getDeclaredConstructor().newInstance();
                    Class<?>[] argumentTypes = new Class[point.getArgs().length];
                    for (int i = 0; i < point.getArgs().length; i++) {
                        argumentTypes[i] = point.getArgs()[i].getClass();
                    }
                    Method method = aClass.getMethod(methodName, argumentTypes);
                    method.invoke(instance, point.getArgs());
                }
            }

        }

        return point.proceed();


    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (PathConstant value : PathConstant.values()) {
            if (value.getType().equals(verifyProperties.getPathType())) {
                pathInterface = SpringUtil.getBean(value.getBeanName());
                break;
            }
        }
        if (pathInterface == null) {
            throw new RuntimeException("未获取到校验路径bean");
        }


    }
}
