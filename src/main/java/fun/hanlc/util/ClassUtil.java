package fun.hanlc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Comparator;

public class ClassUtil {

    static Logger logger = LoggerFactory.getLogger(ClassUtil.class);

    private static CustomClassLoader classLoader = new CustomClassLoader();;

    public  static Class<?> getClass(String classContent, String packageName) throws Exception {

        //1.根据类名在内存中获取
        Class classForJvm = getClassForJvm(packageName);
        if (classForJvm != null) {
            logger.info("从内存中获取类");
            return classForJvm;
        }
        synchronized(CustomClassLoader.class){
            classForJvm = getClassForJvm(packageName);
            if (classForJvm != null) {
                logger.info("从内存中获取类");
                return classForJvm;
            }
            logger.info("{}类文件开始创建",packageName);
            // 2. 创建临时目录和保存类文件的临时Java源文件
            String className = packageName; // 替换为完整的类名（包括包名）
            String javaSource = className.replace('.', '/') + ".java";
            Path tempDir = Files.createTempDirectory("dynamic_class_loading");
            Path packageDir = tempDir.resolve(className.substring(0, className.lastIndexOf('.'))).normalize();
            Files.createDirectories(packageDir);
            Path sourceFile = packageDir.resolve(className.substring(className.lastIndexOf('.') + 1) + ".java");
            Files.write(sourceFile, Collections.singletonList(classContent), StandardCharsets.UTF_8, StandardOpenOption.CREATE);

            // 3. 使用JavaCompiler动态编译Java源文件
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            compiler.run(null, null, null, sourceFile.toString());

            // 4. 将编译后的字节码文件加载到内存中
//        CustomClassLoader classLoader = new CustomClassLoader();
            String classFilePath = getClassFilePath(sourceFile.toString());
            Class<?> loadedClass = classLoader.findClass(classFilePath); // 加载字节码文件
            //删除临时文件
            deleteDirectory(tempDir);
            logger.info("{}类文件加载成功",packageName);
            return loadedClass;
        }

    }

    public static Class getClassForJvm(String className){
        try {
            Class<?> aClass =  classLoader.loadClass(className);
            return aClass;
        } catch (ClassNotFoundException e) {
            return null;
        }

    }


    static class CustomClassLoader extends ClassLoader {
        @Override
        public Class<?> findClass(String name) throws ClassNotFoundException {
            try {
                byte[] classBytes = Files.readAllBytes(Paths.get(name));
                return defineClass(null, classBytes, 0, classBytes.length);
            } catch (IOException e) {
                throw new ClassNotFoundException(name, e);
            }
        }
    }

    private static void deleteDirectory(Path directory) throws IOException {
        if (Files.exists(directory)) {
            Files.walk(directory)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }


    public static boolean isMethodExist(Class<?> clazz, String methodName) {
        // 获取类中声明的所有方法
        Method[] methods = clazz.getDeclaredMethods();

        // 遍历方法数组，查找目标方法
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return true;
            }
        }

        return false;
    }

    private static String getClassFilePath(String javaFilePath) {
        String classFilePath = javaFilePath.replace(".java", ".class");
        return classFilePath;
    }
}
