# verify-spring-boot-starter

#### 介绍
verify-spring-boot-starter 动态参数校验
#### 软件架构
软件架构说明
目前测试过mysql


#### 安装教程

1.  创建数据库表
```sql
CREATE TABLE `sys_verify`  (
                               `id` int(11) NOT NULL,
                               `PACKAGE_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '动态java代码包名',
                               `JAVA_CONTENT` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'java代码内容',
                               `CHECK_PACKAGE_PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验的包名路径 例如 fun.hanlc.xxx',
                               `METHOD_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验的方法名称',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
```


#### 使用说明

1.  创建数据库表
2.  将编写好的代码放入JAVA_CONTENT字段
3.  配置文件新增
```yaml
fun:
  hanlc:
    verify:
    path-type: LIST #路径类型 ONE 只匹配一个路径  LIST 匹配数组路径  ALL匹配所有
    verify-aop-paths[0]: cn.bugstack.middleware.whitelist.test.interfaces.UserController #需要拦截的类名 path-type为LIST时使用
    verify-aop-path: cn.bugstack.middleware.whitelist.test.interfaces.UserController #需要拦截的类名 path-type为ONE时使用
    sys-start: false #是否在程序启动时加载数据库文件并载入
```
4. 开始使用
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
